package com.rsreu.treatment;

import java.util.Scanner;

public class Array {

    private int[] array;
    private int from, to, length;

    Array(int length) {
        this.array = new int[length];
        this.length = length;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public int getTo() {
        return to;
    }

    public boolean rangeCheck() {
        if (this.from >= this.to) {
            return true;
        } else {
            return false;
        }
    }

    private int elementGeneration() {
        return this.from + (int) (Math.random() * ((this.to - this.from) + 1));
    }

    public void autoInput() {
        for (int i = 0; i < this.length; i++) {
            this.array[i] = elementGeneration();
        }
    }

    public void manualInput() {
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < this.length; i++) {
            int temp;
            System.out.print(i + ": ");
            while (!in.hasNextInt()) {
                System.out.print("Vvedite chislo: ");
                in.next();
            }
            temp = in.nextInt();
            this.array[i] = temp;
        }
    }

    public int summationEven() {
        int sum = 0;
        for (int i = 0; i < this.length; i++) {
            if ((i + 1) % 2 == 0) {
                sum += this.array[i];
            }
        }
        return sum;
    }

    public void getArray() {
        for (int i = 0; i < this.length; i++) {
            System.out.print(this.array[i] + ", ");
        }
    }
}
