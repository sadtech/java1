package com.rsreu.treatment;

public class Сalculator {
    private int a, b;

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int sum() {
        return this.a + this.b;
    }

    public int vich() {
        return this.a - this.b;
    }

    public int umnog() {
        return this.a * this.b;
    }

    public int delen() {
        return this.a / this.b;
    }

}
