package com.rsreu.treatment;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.io.IOException;
import java.util.Scanner;

public class Treatment {

    public static void main(String[] args) throws IOException {
        Treatment g = new Treatment();
        g.runTreatment();
    }

    public void runTreatment() throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.print("\n\n1 - Task1\n2 - Task2\n3 - Task3\n4 - Vihod\nViberete zadanie: ");
        while (!in.hasNextInt()) {
            in.next();
        }
        switch (in.nextInt()) {
            case 1:
                initializationArray();
                break;
            case 2:
                initializationStrok();
                break;
            case 3:
                initializationCalculator();
                break;
            case 4:
                System.exit(1);
            default:
                System.out.print("Vvedeno nekorrektnoe znachenie!\n\n");
                runTreatment();
                break;
        }
    }

    public void initializationArray() throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.print("Vvedite kolichestvo elementov: ");
        int lengthArray = in.nextInt();
        Array arr = new Array(lengthArray);
        System.out.print("Ispolzovat avto vvod? Y-Yes/N-no: ");
        char pro = (char) System.in.read();
        if (pro == 'Y' || pro == 'y') {
            do {
                System.out.print("Vvedite nachalo diapazona: ");
                while (!in.hasNextInt()) {
                    in.next();
                }
                arr.setFrom(in.nextInt());
                System.out.print("Vvedite konec diapazona: ");
                while (!in.hasNextInt()) {
                    in.next();
                }
                arr.setTo(in.nextInt());
            } while (arr.rangeCheck());
            arr.autoInput();
        } else {
            arr.manualInput();
        }
        arr.getArray();
        System.out.println("SUMMA: " + arr.summationEven());
        runTreatment();
    }

    public void initializationStrok() throws IOException {
        System.out.print("Vvedite kolichestvo strok: ");
        Scanner in = new Scanner(System.in);
        while (!in.hasNextInt()) {
            in.next();
        }
        Stroka str = new Stroka(in.nextInt());
        str.inpitStrok();
        System.out.println("\nRezult:");
        str.searchStroka();
        runTreatment();
    }

    public void initializationCalculator() throws IOException {
        Сalculator calc = new Сalculator();
        Scanner in = new Scanner(System.in);
        System.out.print("Vvedite A: ");
        while (!in.hasNextInt()) {
            in.next();
        }
        calc.setA(in.nextInt());
        System.out.print("Vvedite B: ");
        while (!in.hasNextInt()) {
            in.next();
        }
        calc.setB(in.nextInt());
        System.out.print("Vvedite operator: ");
        char oper = (char) System.in.read();
        System.out.print("Rezult: ");
        switch (oper) {
            case '+':
                System.out.println(calc.sum());
                break;
            case '/':
                System.out.println(calc.delen());
                break;
            case '*':
                System.out.println(calc.umnog());
                break;
            case '-':
                System.out.println(calc.vich());
                break;
            default:
                initializationCalculator();
        }
        runTreatment();
    }
}