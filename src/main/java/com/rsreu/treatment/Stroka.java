package com.rsreu.treatment;

import java.lang.String;
import java.util.Scanner;

public class Stroka {

    private String[] str;

    Stroka(int count) {
        this.str = new String[count];
    }

    public void inpitStrok() {
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < this.str.length; i++) {
            System.out.print("Vvedite " + (i + 1) + "-oy stroku: ");
            this.str[i] = in.nextLine();
        }
    }

    private int avg() {
        int avg = 0;
        for (int i = 0; i < this.str.length; i++) {
            avg += this.str[i].length();
        }
        return avg / this.str.length;
    }

    public void searchStroka() {
        String rezult;
        int avg = this.avg();
        for (int i = 0; i < this.str.length; i++) {
            if (this.str[i].length() > avg) {
                System.out.println(this.str[i] + "(" + this.str[i].length() + ")");
            }
        }
    }
}
